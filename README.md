Demo PaaS avec SUSE Cloud Application Platform
==============================================

Installation
------------

Le dossier `server` contient les scripts à lancer depuis le noeud cassp-admin

Modifier les fichiers :
  * scf-config-values.yaml
  * master_nodes.lst
  * worker_nodes.lst  

Le dossier `client` contient les scripts à lancer depuis la machine du "developpeur"
