#!/bin/bash
bold=$(tput bold)
normal=$(tput sgr0)

. ./00-vars.sh

while true; do
   echo -e "${bold}---\nInstallation de helm version 3 dans ~/bin/${normal}"
   echo -e " ${bold}Commande :${normal} curl -O https://get.helm.sh/helm-v${HELM_VERSION}-linux-amd64.tar.gz"
   read -p " ${bold}Executer ? (y/n) ${normal}" yn
   echo
   case $yn in
      [Yy]* )
         echo "Execution en cours..."
  curl -O https://get.helm.sh/helm-v${HELM_VERSION}-linux-amd64.tar.gz
  tar zxvf helm-v${HELM_VERSION}-linux-amd64.tar.gz
  mv linux-amd64/helm ~/bin/helm3
  rm -rf linux-amd64/
  rm helm-v${HELM_VERSION}-linux-amd64.tar.gz
         break;;
      [Nn]* ) echo "Etape annulee";break;;
        * ) echo "Please answer yes (y) or no (n).";;
    esac
done

while true; do
   echo -e "${bold}---\nAjout du repo Helm Chart SUSE${normal}"
   echo -e " ${bold}Commande :${normal} helm3 repo add suse https://kubernetes-charts.suse.com/"
   read -p " ${bold}Executer ? (y/n) ${normal}" yn
   echo
   case $yn in
      [Yy]* )
         echo "Execution en cours..."
            helm3 repo add suse https://kubernetes-charts.suse.com/
         break;;
      [Nn]* ) echo "Etape annulee";break;;
        * ) echo "Please answer yes (y) or no (n).";;
    esac
done

while true; do
   echo -e "${bold}---\nInstaller le service nginx-ingress générique${normal}"
   echo -e " ${bold}Commande :${normal} helm3 install nginx-ingress suse/nginx-ingress"
   read -p " ${bold}Executer ? (y/n) ${normal}" yn
   echo
   case $yn in
      [Yy]* )
         echo "Execution en cours..."
            helm3 install nginx-ingress suse/nginx-ingress
         break;;
      [Nn]* ) echo "Etape annulee";break;;
        * ) echo "Please answer yes (y) or no (n).";;
    esac
done

echo
echo -e "${bold}---\nEntrez le nom de domain pour CAP.\nOu laissez vide pour créer un nom de domain en xip.io pour le cloud public${normal}"
read -p "${bold}Nom de domaine :${normal}" SYSTEM_DOMAIN
if [[ -z $SYSTEM_DOMAIN ]]; then
   echo "Création d'un nom à partir de l'IP du LoadBalancer"
   EXTERNAL_IP=$(kubectl get svc nginx-ingress-controller | awk '{print$4}'  | tail -1)
   SYSTEM_DOMAIN="${EXTERNAL_IP}.xip.io"
fi

echo "---"
echo "Nom de domain : $SYSTEM_DOMAIN"
echo "---"

while true; do
   echo -e "${bold}---\nGenéré un certificat pour le domaine $SYSTEM_DOMAIN ${normal}"
   echo -e " ${bold}Commande :${normal} openssl req -newkey rsa:1024 -sha256 -nodes -out Server.csr -keyout Server.key -config req.cfg -subj \"/CN=$SYSTEM_DOMAIN/\""
   read -p " ${bold}Executer ? (y/n) ${normal}" yn
   echo
   case $yn in
      [Yy]* )
         echo "Execution en cours..."

cat <<EOF > req.cfg
[req]
distinguished_name = req_distinguished_name
req_extensions     = req_ext
[req_distinguished_name]

[req_ext]
subjectAltName = @alt_names
[alt_names]
DNS.1 = *.$SYSTEM_DOMAIN
DNS.2 = *.uaa.$SYSTEM_DOMAIN
DNS.3 = uaa.$SYSTEM_DOMAIN
DNS.4 = scf.$SYSTEM_DOMAIN
DNS.5 = uaa
DNS.6 = scf
DNS.7 = scf.uaa.$SYSTEM_DOMAIN
EOF

openssl req -x509 -newkey rsa:1024 -sha256 -nodes -days 1826 -keyout RootCA.key -out RootCA.crt
openssl req -newkey rsa:1024 -sha256 -nodes -out Server.csr -keyout Server.key -config req.cfg -subj "/CN=$SYSTEM_DOMAIN/"
openssl x509 -req -days 1000  -extfile req.cfg -extensions req_ext -in Server.csr -out Server.crt -CA RootCA.crt -CAkey RootCA.key -CAcreateserial

         break;;
      [Nn]* ) echo "Etape annulee";break;;
        * ) echo "Please answer yes (y) or no (n).";;
    esac
done
echo

echo "Les fichiers Server.crt et Server.key ont été crées"
ls -l Server.crt Server.key

echo
echo "Storage Class disponible (block RWO prefered)"
kubectl get sc
echo 

while true; do
   echo -e "${bold}---\nDefinir une nouvelle Storage Class par defautl${normal}"
   echo -e " ${bold}Commande :${normal} kubectl patch storageclass <SC> -p '{\"metadata\": {\"annotations\":{\"storageclass.kubernetes.io/is-default-class\":\"true\"}}}'"
   read -p " ${bold}Executer ? (y/n) ${normal}" yn
   echo
   case $yn in
      [Yy]* )
         echo "Execution en cours..."
            read -p " ${bold}Nom de la storage claas (première colone) :${normal}" sc
            kubectl patch storageclass $sc -p '{"metadata": {"annotations":{"storageclass.kubernetes.io/is-default-class":"true"}}}'
         break;;
      [Nn]* ) echo "Etape annulee";break;;
        * ) echo "Please answer yes (y) or no (n).";;
    esac
done

while true; do
   echo -e "${bold}---\nGenéré le fichier values pour l'installation via helm3 de CAP ${normal}"
   echo -e """ ${bold}Contenu :${normal} 
system_domain: $SYSTEM_DOMAIN
credentials:
  cf_admin_password: changeme
  uaa_admin_client_secret: alsochangeme
features:
  ingress:
    enabled: true
    tls:
      crt: |
      <Contenut de Server.crt>
      key: |
      <Contenut de Server.key>
   """
   read -p " ${bold}Executer ? (y/n) ${normal}" yn
   echo
   case $yn in
      [Yy]* )
         echo "Execution en cours..."

cat <<EOF > values.yaml
system_domain: $SYSTEM_DOMAIN
credentials:
  cf_admin_password: changeme
  uaa_admin_client_secret: alsochangeme
features:
  ingress:
    enabled: true
    tls:
      crt: |
EOF

cat Server.crt | sed 's/^/        /' >> values.yaml
echo "      key: |" >> values.yaml
cat Server.key | sed 's/^/        /' >> values.yaml

         break;;
      [Nn]* ) echo "Etape annulee";break;;
        * ) echo "Please answer yes (y) or no (n).";;
    esac
done
echo

while true; do
   echo -e "${bold}---\nInstallation de l'operator CF via helm3 ${normal}"
   echo -e " ${bold}Commande :${normal} helm3 install cf-operator suse/cf-operator --namespace cf-operator --set \"global.operator.watchNamespace=kubecf\""
   read -p " ${bold}Executer ? (y/n) ${normal}" yn
   echo
   case $yn in
      [Yy]* )
         echo "Execution en cours..."

kubectl create ns cf-operator
helm3 install cf-operator suse/cf-operator --namespace cf-operator --set "global.operator.watchNamespace=kubecf" --version ${CF_OPERATOR_VERSION}

         break;;
      [Nn]* ) echo "Etape annulee";break;;
        * ) echo "Please answer yes (y) or no (n).";;
    esac
done
echo

watch -d "kubectl top nodes; kubectl get pods -n cf-operator -o wide"

while true; do
   echo -e "${bold}---\nInstallation de CAP avec le fichier values.yaml via helm3 ${normal}"
   echo -e " ${bold}Commande :${normal} helm3 install kubecf suse/kubecf --namespace kubecf --values values.yaml --version ${KUBECF_VERSION}"
   read -p " ${bold}Executer ? (y/n) ${normal}" yn
   echo
   case $yn in
      [Yy]* )
         echo "Execution en cours..."

helm3 install kubecf suse/kubecf --namespace kubecf --values values.yaml --version ${KUBECF_VERSION}

        break;;
      [Nn]* ) echo "Etape annulee";break;;
        * ) echo "Please answer yes (y) or no (n).";;
    esac
done
echo

watch -d "kubectl top nodes; kubectl get pvc -n kubecf; kubectl get pods -n kubecf -o wide"

while true; do
   echo -e "${bold}---\nInstallation de cf-cli dans ~/bin/${normal}"
   echo -e " ${bold}Commande :${normal} curl -O https://packages.cloudfoundry.org/stable?release=linux64-binary"
   read -p " ${bold}Executer ? (y/n) ${normal}" yn
   echo
   case $yn in
      [Yy]* )
         echo "Execution en cours..."
  curl -Lo cf-cli-6.37.0.tar.gz 'https://packages.cloudfoundry.org/stable?release=linux64-binary&version=6.37.0&source=github-rel'
  tar zxvf cf-cli-6.37.0.tar.gz cf
  rm cf-cli-6.37.0.tar.gz
  mv cf ~/bin/cf
         break;;
      [Nn]* ) echo "Etape annulee";break;;
        * ) echo "Please answer yes (y) or no (n).";;
    esac
done

while true; do
   echo -e "${bold}---\nConnexion à l'API CF avec le compte admin (mdp : changeme)${normal}"
   echo -e " ${bold}Commande :${normal} cf api --skip-ssl-validation https://api.${SYSTEM_DOMAIN} \ncf login"
   read -p " ${bold}Executer ? (y/n) ${normal}" yn
   echo
   case $yn in
      [Yy]* )
         echo "Execution en cours..."
cf api --skip-ssl-validation https://api.${SYSTEM_DOMAIN} 
cf login
         break;;
      [Nn]* ) echo "Etape annulee";break;;
        * ) echo "Please answer yes (y) or no (n).";;
    esac
done

while true; do
   echo -e "${bold}---\nCreation d'un premier SPACE${normal}"
   echo -e " ${bold}Commande :${normal} cf create-space space"
   read -p " ${bold}Executer ? (y/n) ${normal}" yn
   echo
   case $yn in
      [Yy]* )
         echo "Execution en cours..."
cf create-space space
cf target -s space
         break;;
      [Nn]* ) echo "Etape annulee";break;;
        * ) echo "Please answer yes (y) or no (n).";;
    esac
done

while true; do
   echo -e "${bold}---\nInstallation de minibroker (service mariadb, postgre, redis, mongodb) ${normal}"
   echo -e " ${bold}Commande :${normal} kubectl create namespace minibroker\nhelm3 install minibroker suse/minibroker"
   read -p " ${bold}Executer ? (y/n) ${normal}" yn
   echo
   case $yn in
      [Yy]* )
         echo "Execution en cours..."

kubectl create namespace minibroker

helm3 install minibroker suse/minibroker \
--namespace minibroker \
--set "defaultNamespace=minibroker"

        break;;
      [Nn]* ) echo "Etape annulee";break;;
        * ) echo "Please answer yes (y) or no (n).";;
    esac
done
echo

watch 'kubectl get pods --namespace minibroker'

while true; do
   echo -e "${bold}---\nEnregistrement de minibroker-redis dans la marketplace CF${normal}"
   echo -e " ${bold}Commande :${normal} cf create-service-broker minibroker\ncf enable-service-access redis"
   read -p " ${bold}Executer ? (y/n) ${normal}" yn
   echo
   case $yn in
      [Yy]* )
         echo "Execution en cours..."
cf create-service-broker minibroker username password http://minibroker-minibroker.minibroker.svc.cluster.local
cf enable-service-access redis -b minibroker -p 4-0-10
         break;;
      [Nn]* ) echo "Etape annulee";break;;
        * ) echo "Please answer yes (y) or no (n).";;
    esac
done
